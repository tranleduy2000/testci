package com.duy.simple.calculator.ui;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.duy.automationtest.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class CalculatorActivityTest {

    @Rule
    public ActivityScenarioRule<CalculatorActivity> rule = new ActivityScenarioRule<>(CalculatorActivity.class);

    @Test
    public void testAddTwoNumbers() {

        onView(withId(R.id.txt_number_a)).perform(click()).perform(typeText("12"));
        onView(withId(R.id.txt_number_b)).perform(click()).perform(typeText("23"));

        onView(withId(R.id.btn_addition)).perform(click());

        onView(withId(R.id.txt_result)).check(matches(withText("35")));
        InstrumentationRegistry.getInstrumentation().waitForIdleSync();;
    }

    @Test
    public void testSubtractTwoNumbers() {

        onView(withId(R.id.txt_number_a)).perform(click()).perform(typeText("12.3"));
        onView(withId(R.id.txt_number_b)).perform(click()).perform(typeText("23.1"));

        onView(withId(R.id.btn_subtraction)).perform(click());

        onView(withId(R.id.txt_result)).check(matches(withText("-10.8")));
    }

    @Test
    public void testMultiplyTwoNumbers() {

        onView(withId(R.id.txt_number_a)).perform(click()).perform(typeText("4"));
        onView(withId(R.id.txt_number_b)).perform(click()).perform(typeText("3"));

        onView(withId(R.id.btn_multiplication)).perform(click());

        onView(withId(R.id.txt_result)).check(matches(withText("12")));
    }

    @Test
    public void testDivideTwoNumbers() {

        onView(withId(R.id.txt_number_a)).perform(click()).perform(typeText("12"));
        onView(withId(R.id.txt_number_b)).perform(click()).perform(typeText("4"));

        onView(withId(R.id.btn_division)).perform(click());

        onView(withId(R.id.txt_result)).check(matches(withText("3")));

    }

    @Test
    public void testDivideByZeroError() {
        onView(withId(R.id.txt_number_a)).perform(click()).perform(typeText("1"));
        onView(withId(R.id.txt_number_b)).perform(click()).perform(typeText("0"));

        onView(withId(R.id.btn_division)).perform(click());

        onView(withText("Division by zero")).check(matches(isDisplayed()));
    }
}