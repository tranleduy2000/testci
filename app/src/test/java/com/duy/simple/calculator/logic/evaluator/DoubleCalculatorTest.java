package com.duy.simple.calculator.logic.evaluator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DoubleCalculatorTest {
    private final DoubleCalculator calculator = new DoubleCalculator();


    @Test
    public void testAdd() {
        assertEquals((Double) 3.0,
                calculator.add(1d, 2d));
        assertEquals((Double) 3.5,
                calculator.add(1.2, 2.3));
    }

    @Test
    public void testSubtract() {
        assertEquals((Double) 2d,
                calculator.subtract(3d, 1d));

        assertEquals((Double) (-1.1),
                calculator.subtract(0.9, 2d));

    }

    @Test
    public void testMultiply() {
        assertEquals((Double) 6d,
                calculator.multiply(2d, 3d));
        assertEquals((Double) 4.08,
                calculator.multiply(1.2, 3.4));
    }

    @Test
    public void testDivide() {
        assertEquals((Double) 0.6666666666666666666666666666666667,
                calculator.divide(2.0, 3.0));
    }

    @Test(expected = ArithmeticException.class)
    public void testDivideByZero() {
        calculator.divide(2.0, 0.0);
    }
}