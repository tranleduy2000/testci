package com.duy.simple.calculator.logic.evaluator;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class BigDecimalCalculatorTest {

   private final BigDecimalCalculator bigDecimalCalculator = new BigDecimalCalculator();

    @Test
    public void testAdd() {
        assertEquals(new BigDecimal("3"),
                bigDecimalCalculator.add(new BigDecimal("1"), new BigDecimal("2")));
        assertEquals(new BigDecimal("3.5"),
                bigDecimalCalculator.add(new BigDecimal("1.2"), new BigDecimal("2.3")));
    }

    @Test
    public void testSubtract() {
        assertEquals(new BigDecimal("2"),
                bigDecimalCalculator.subtract(new BigDecimal("3"), new BigDecimal("1")));
        assertEquals(new BigDecimal("-1.1"),
                bigDecimalCalculator.subtract(new BigDecimal("0.9"), new BigDecimal("2")));

    }

    @Test
    public void testMultiply() {
        assertEquals(new BigDecimal("6"),
                bigDecimalCalculator.multiply(new BigDecimal("2"), new BigDecimal("3")));
        assertEquals(new BigDecimal("4.08"),
                bigDecimalCalculator.multiply(new BigDecimal("1.2"), new BigDecimal("3.4")));
    }

    @Test
    public void testDivide() {
        assertEquals(new BigDecimal("0.6666666666666666666666666666666667"),
                bigDecimalCalculator.divide(new BigDecimal("2"), new BigDecimal("3")));
    }

    @Test(expected = ArithmeticException.class)
    public void testDivideByZero() {
        bigDecimalCalculator.divide(new BigDecimal("2"), BigDecimal.ZERO);
    }
}