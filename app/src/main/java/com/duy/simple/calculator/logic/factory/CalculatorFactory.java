package com.duy.simple.calculator.logic.factory;

import com.duy.simple.calculator.logic.evaluator.ICalculator;
import com.duy.simple.calculator.logic.evaluator.BigDecimalCalculator;
import com.duy.simple.calculator.logic.evaluator.DoubleCalculator;

import java.math.BigDecimal;

public class CalculatorFactory implements ICalculatorFactory {


    @SuppressWarnings("unchecked")
    @Override
    public <T> ICalculator<T> getCalculator(Class<?> type) {

        if (type.equals(BigDecimal.class)) {
            return (ICalculator<T>) BigDecimalCalculator.getInstance();

        } else if (type.equals(Double.class)) {
            return (ICalculator<T>) DoubleCalculator.getInstance();

        } else {
            throw new UnsupportedOperationException();
        }
    }
}
