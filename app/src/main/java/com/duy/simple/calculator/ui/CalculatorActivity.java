package com.duy.simple.calculator.ui;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.duy.automationtest.R;
import com.duy.simple.calculator.logic.factory.CalculatorFactory;
import com.duy.simple.calculator.logic.evaluator.ICalculator;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.function.BiFunction;

public class CalculatorActivity extends AppCompatActivity {

    private ICalculator<BigDecimal> calculator;

    private EditText txtNumberA;
    private EditText txtNumberB;
    private TextView txtResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupCalculator();

        setupViews();

    }

    private void setupCalculator() {
        CalculatorFactory calculatorFactory = new CalculatorFactory();
        this.calculator = calculatorFactory.getCalculator(BigDecimal.class);
    }

    private void setupViews() {
        txtNumberA = findViewById(R.id.txt_number_a);
        txtNumberB = findViewById(R.id.txt_number_b);
        txtResult = findViewById(R.id.txt_result);

        findViewById(R.id.btn_addition).setOnClickListener(v -> {
            performCalculate((a, b) -> calculator.add(a, b));
        });
        findViewById(R.id.btn_subtraction).setOnClickListener(v -> {
            performCalculate((a, b) -> calculator.subtract(a, b));
        });
        findViewById(R.id.btn_multiplication).setOnClickListener(v -> {
            performCalculate((a, b) -> calculator.multiply(a, b));
        });
        findViewById(R.id.btn_division).setOnClickListener(v -> {
            performCalculate((a, b) -> calculator.divide(a, b));
        });
    }

    private void performCalculate(BiFunction<BigDecimal, BigDecimal, BigDecimal> operation) {
        try {
            BigDecimal numberA = new BigDecimal(txtNumberA.getText().toString());
            BigDecimal numberB = new BigDecimal(txtNumberB.getText().toString());
            BigDecimal result = operation.apply(numberA, numberB);
            DecimalFormat decimalFormat = new DecimalFormat("#.####", DecimalFormatSymbols.getInstance(Locale.US));
            txtResult.setText(decimalFormat.format(result));
        } catch (Exception e) {
            handleError(e);
        }
    }

    private void handleError(Exception e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error")
                .setMessage(e.getMessage());
        builder.create()
                .show();
    }

}