package com.duy.simple.calculator.logic.factory;

import com.duy.simple.calculator.logic.evaluator.ICalculator;

public interface ICalculatorFactory {

    <T> ICalculator<T> getCalculator(Class<?> type);
}
