package com.duy.simple.calculator.logic.evaluator;

import java.math.BigDecimal;
import java.math.MathContext;

public class BigDecimalCalculator implements ICalculator<BigDecimal> {

    private static BigDecimalCalculator instance;

    public static BigDecimalCalculator getInstance() {
        if (instance == null) {
            instance = new BigDecimalCalculator();
        }
        return instance;
    }

    /*package private*/ BigDecimalCalculator() {

    }

    @Override
    public BigDecimal add(BigDecimal a, BigDecimal b) {
        return a.add(b);
    }

    @Override
    public BigDecimal subtract(BigDecimal a, BigDecimal b) {
        return a.subtract(b);
    }

    @Override
    public BigDecimal multiply(BigDecimal a, BigDecimal b) {
        return a.multiply(b);
    }

    @Override
    public BigDecimal divide(BigDecimal a, BigDecimal b) {
        return a.divide(b, MathContext.DECIMAL128);
    }
}
