package com.duy.simple.calculator.logic.evaluator;

public class DoubleCalculator implements ICalculator<Double> {
    private static DoubleCalculator instance;

    public static DoubleCalculator getInstance() {
        if (instance == null) {
            instance = new DoubleCalculator();
        }
        return instance;
    }

    /*package private*/ DoubleCalculator() {

    }

    @Override
    public Double add(Double a, Double b) {
        return a + b;
    }

    @Override
    public Double subtract(Double a, Double b) {
        return a - b;
    }

    @Override
    public Double multiply(Double a, Double b) {
        return a * b;
    }

    @Override
    public Double divide(Double a, Double b) {
        if (b == 0.0) {
            throw new ArithmeticException("Division by zero");
        }
        return a / b;
    }
}
